import math
import json
import collections

data_json = """{
  "R1": {
    "1":{
      "point": 1,
      "sector": 3,
      "signal": -49
    },
    "2":{
      "point": 2,
      "sector": 3,
      "signal": -33
    },
    "3":{
      "point": 3,
      "sector": 3,
      "signal": -56
    },
    "4":{
      "point": 4,
      "sector": 3,
      "signal": -45
    },
    "5":{
      "point": 5,
      "sector": 2,
      "signal": -76
    },
    "6":{
      "point": 6,
      "sector": 2,
      "signal": -74
    },
    "7":{
      "point": 7,
      "sector": 2,
      "signal": -73
    },
    "8":{
      "point": 8,
      "sector": 2,
      "signal": -72
    },
    "9":{
      "point": 9,
      "sector": 1,
      "signal": -94
    },
    "10":{
      "point": 10,
      "sector": 1,
      "signal": -88
    },
    "11":{
      "point": 11,
      "sector": 1,
      "signal": -88
    },
    "12":{
      "point": 12,
      "sector": 1,
      "signal": -81
    }
  },
  "R2": {
    "1":{
      "point": 1,
      "sector": 3,
      "signal": -74
    },
    "2":{
      "point": 2,
      "sector": 3,
      "signal": -73
    },
    "3":{
      "point": 3,
      "sector": 3,
      "signal": -65
    },
    "4":{
      "point": 4,
      "sector": 3,
      "signal": -75
    },
    "5":{
      "point": 5,
      "sector": 2,
      "signal": -68
    },
    "6":{
      "point": 6,
      "sector": 2,
      "signal": -33
    },
    "7":{
      "point": 7,
      "sector": 2,
      "signal": -53
    },
    "8":{
      "point": 8,
      "sector": 2,
      "signal": -55
    },
    "9":{
      "point": 9,
      "sector": 1,
      "signal": -68
    },
    "10":{
      "point": 10,
      "sector": 1,
      "signal": -78
    },
    "11":{
      "point": 11,
      "sector": 1,
      "signal": -68
    },
    "12":{
      "point": 12,
      "sector": 1,
      "signal": -64
    }
  },
  "R3": {
    "1":{
      "point": 1,
      "sector": 3,
      "signal": -87
    },
    "2":{
      "point": 2,
      "sector": 3,
      "signal": -90
    },
    "3":{
      "point": 3,
      "sector": 3,
      "signal": -89
    },
    "4":{
      "point": 4,
      "sector": 3,
      "signal": -79
    },
    "5":{
      "point": 5,
      "sector": 2,
      "signal": -81
    },
    "6":{
      "point": 6,
      "sector": 2,
      "signal": -88
    },
    "7":{
      "point": 7,
      "sector": 2,
      "signal": -80
    },
    "8":{
      "point": 8,
      "sector": 2,
      "signal": -66
    },
    "9":{
      "point": 9,
      "sector": 1,
      "signal": -62
    },
    "10":{
      "point": 10,
      "sector": 1,
      "signal": -30
    },
    "11":{
      "point": 11,
      "sector": 1,
      "signal": -53
    },
    "12":{
      "point": 12,
      "sector": 1,
      "signal": -54
    }
  }
}"""

data = json.loads(data_json)

measure_json = """{ "R1": -90, "R2":-55, "R3": -5 }"""

measure = json.loads(measure_json)

info_json = """{
  "routers": ["R1", "R2", "R3"],
  "points": [1,2,3,4,5,6,7,8,9,10,11,12]
}"""

info = json.loads(info_json)

result = 0

leght = {}

k = 4

for i in info["points"]:
    for x in info["routers"]:
        result += ((data[str(x)][str(i)]["signal"])-(measure[str(x)]))**2
    leght[i] = math.sqrt(result)
    result = 0
list_of_point = list()
list_of_items = leght.items()
for x in (sorted(leght.values()))[:k]:
    for item in list_of_items:
        if item[1] == x:
            list_of_point.append(item[0])

print(list_of_point)

list_of_sectors = list()

for point in list_of_point:
    list_of_sectors.append(data["R1"][str(point)]["sector"])

print(list_of_sectors)

counter = collections.Counter(list_of_sectors)
print(list(counter.keys())[0])



mycol.insert_many([
 {"_id": 1, "R1": [{
        "1": {
          "point": 1,
          "sector": 3,
          "signal": -49
        },
        "2": {
          "point": 2,
          "sector": 3,
          "signal": -33
        },
        "3": {
          "point": 3,
          "sector": 3,
          "signal": -56
        },
        "4": {
          "point": 4,
          "sector": 3,
          "signal": -45
        },
        "5": {
          "point": 5,
          "sector": 2,
          "signal": -76
        },
        "6": {
          "point": 6,
          "sector": 2,
          "signal": -74
        },
        "7": {
          "point": 7,
          "sector": 2,
          "signal": -73
        },
        "8": {
          "point": 8,
          "sector": 2,
          "signal": -72
        },
        "9": {
          "point": 9,
          "sector": 1,
          "signal": -94
        },
        "10": {
          "point": 10,
          "sector": 1,
          "signal": -88
        },
        "11": {
          "point": 11,
          "sector": 1,
          "signal": -88
        },
        "12": {
          "point": 12,
          "sector": 1,
          "signal": -81
        }
    }]
  },
 {"_id": 2, "R2": [{
        "1": {
          "point": 1,
          "sector": 3,
          "signal": -74
        },
        "2": {
          "point": 2,
          "sector": 3,
          "signal": -73
        },
        "3": {
          "point": 3,
          "sector": 3,
          "signal": -65
        },
        "4": {
          "point": 4,
          "sector": 3,
          "signal": -75
        },
        "5": {
          "point": 5,
          "sector": 2,
          "signal": -68
        },
        "6": {
          "point": 6,
          "sector": 2,
          "signal": -33
        },
        "7": {
          "point": 7,
          "sector": 2,
          "signal": -53
        },
        "8": {
          "point": 8,
          "sector": 2,
          "signal": -55
        },
        "9": {
          "point": 9,
          "sector": 1,
          "signal": -68
        },
        "10": {
          "point": 10,
          "sector": 1,
          "signal": -78
        },
        "11": {
          "point": 11,
          "sector": 1,
          "signal": -68
        },
        "12": {
          "point": 12,
          "sector": 1,
          "signal": -64
        }
    }]
  },
 {"_id": 3, "R3": [{
        "1": {
          "point": 1,
          "sector": 3,
          "signal": -87
        },
        "2": {
          "point": 2,
          "sector": 3,
          "signal": -90
        },
        "3": {
          "point": 3,
          "sector": 3,
          "signal": -89
        },
        "4": {
          "point": 4,
          "sector": 3,
          "signal": -79
        },
        "5": {
          "point": 5,
          "sector": 2,
          "signal": -81
        },
        "6": {
          "point": 6,
          "sector": 2,
          "signal": -88
        },
        "7": {
          "point": 7,
          "sector": 2,
          "signal": -80
        },
        "8": {
          "point": 8,
          "sector": 2,
          "signal": -66
        },
        "9": {
          "point": 9,
          "sector": 1,
          "signal": -62
        },
        "10": {
          "point": 10,
          "sector": 1,
          "signal": -30
        },
        "11": {
          "point": 11,
          "sector": 1,
          "signal": -53
        },
        "12": {
          "point": 12,
          "sector": 1,
          "signal": -54
        }
    }]
  }
])