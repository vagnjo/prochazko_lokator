import pymongo
import json
import webside

client = pymongo.MongoClient("mongodb://127.0.0.1:27017/")

database = client["routers"]
id_database = client["id_list"]
setup_list_of_id = {"_id": "origin", "list": [1]}


def new_id():
    db_id = id_database["id_list"]
    data_id = db_id.find_one()
    if data_id is None:
        global setup_list_of_id
        db_id.insert_one(setup_list_of_id)
        return 1
    else:
        list_id = data_id["list"]
        last_id = (sorted(list_id))[0]
        id_new = last_id + 1
        db_id.update_one({}, {"$push": {"list": id_new}})
        return id_new


def insert(router, signal, sector, _id):
    data_to_insert = {
          "_id": _id,
          "sector": sector,
          "signal": signal
        }
    database[str(router)].insert_one(data_to_insert)


def read_id_list():
    db_id = id_database["id_list"]
    data_id = db_id.find_one()
    return data_id["list"]


def prepare_data(list_of_routers):
    prepared_data = {}
    list_of_data_from_db = []
    for collection in list_of_routers:
        for y in database[str(collection)].find():
            list_of_data_from_db.append(y)
        prepared_data[str(collection)] = list_of_data_from_db
        list_of_data_from_db = []
    return prepared_data


old_data = open("edit_point.json", "r")
if open("edit_point.json", "r") != old_data:
    readfile = open("edit_point.json", "r")
    data = json.loads(readfile.read())
    old_sector = webside.sector
    webside.esp = True
    while old_sector == webside.sector:
        pass
    _id = new_id()
    for routers in data:
        insert(routers, data[str(routers)], webside.sector, _id)
