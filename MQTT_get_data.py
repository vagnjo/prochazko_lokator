import paho.mqtt.client as mqtt
import json
import KNN

mqtt_username = "Ubuntu"
mqtt_password = "31415926"
mqtt_topic = "esp_data"
mqtt_broker_ip = "host"

client = mqtt.Client()
client.username_pw_set(mqtt_username, mqtt_password)


def on_connect(client, userdata, flags, rc):
    client.subscribe(mqtt_topic, qos=1)


def on_message(client, userdata, msg):
    topic = msg.topic
    m_measure = str(msg.payload.decode("utf-8", "ignore"))
    measure = json.loads(m_measure)
    KNN.algorithm(measure)


client.on_connect = on_connect
client.on_message = on_message
client.connect(mqtt_broker_ip, 1883)
client.loop_forever()
