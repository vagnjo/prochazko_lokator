from flask import Flask, render_template, url_for, redirect
from forms import EditForm

esp = 0
sector = 0

web = Flask('app')

web.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'


@web.route('/', methods=['GET', 'POST'])
def edit_point():
	global esp
	global sector
	if esp:
		form = EditForm()
		if form.validate_on_submit():
			sector = form.sector
			print(sector)
			esp = False
		return render_template('edit_point.html', form=form)
	else:
		return redirect(url_for('show_data'))


@web.route('/data', methods=['GET'])
def show_data():
	file = open("final.txt", "r")
	return render_template('show_data.html', posts=file.read())


@web.errorhandler(404)
def page_not_found():
	return render_template('notFound.html'), 404
