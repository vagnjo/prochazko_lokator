import paho.mqtt.client as mqtt
import json

mqtt_username = "Ubuntu"
mqtt_password = "31415926"
mqtt_topic = "esp_edit_point"
mqtt_broker_ip = "192.168.1.103"

client = mqtt.Client()
client.username_pw_set(mqtt_username, mqtt_password)


def on_connect(client, userdata, flags, rc):
    client.subscribe(mqtt_topic, qos=1)


def on_message(client, userdata, msg):
    topic = msg.topic
    file = open("edit_point.json", "w")
    file.write(json.dumps(json.loads(msg.payload.decode("utf-8", "ignore"))))
    file.close()
    readfile = open("edit_point.json", "r")
    data = json.loads(readfile.read())
    print(data)
    print(type(data))
    for x in data:
        print(data[str(x)])


client.on_connect = on_connect
client.on_message = on_message
client.connect(mqtt_broker_ip, 1883)
client.loop_forever()
