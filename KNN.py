import math
import collections
import database

prepared_data = {}


def search_signal(number, router):
    searched_data = [element for element in prepared_data[str(router)] if element["_id"] == number]
    return searched_data[0]["signal"]


def search_sector(point, routers):
    sector = None
    var = None
    for test in routers:
        try:
            var = [element for element in prepared_data[str(test)] if element["_id"] == point]
        finally:
            sector = var[0]["sector"]
    return sector


def algorithm(measure):
    global prepared_data
    prepared_data = database.prepare_data(list(measure.keys()))
    result = 0
    length = {}

    k = 4

    for i in database.read_id_list():
        for x in measure:
            result += (search_signal(i, x)-(measure[str(x)]))**2
        length[i] = math.sqrt(result)
        result = 0

    list_of_point = list()
    list_of_items = length.items()

    for x in (sorted(length.values()))[:k]:
        for item in list_of_items:
            if item[1] == x:
                list_of_point.append(item[0])

    # print(list_of_point)

    list_of_sectors = list()

    for point in list_of_point:
        list_of_sectors.append(search_sector(point, list(measure.keys())))

    # print(list_of_sectors)

    counter = collections.Counter(list_of_sectors)
    final = open("final.txt", "w")
    final.write(list(counter.keys())[0])
    # print(list(counter.keys())[0])
